# CineList


## Tools

Android Studio (Kotlin)<br>
Imdb API (https://imdb-api.com)<br>
Dependencies: Retrofit2, Picasso (Image Loader)


## Features

✓ Menu (Tabs with Fragments instead of classical menu burger looking)<br>
✓ Custom Views<br>
✓ Lists (RecyclerView without dynamic data)<br>
✓ UI with ConstraintLayout and more<br>
✓ Persistence: Data storage<br>
✓ Third-party: Api and more (Search)


## Contributors

LACHAISE Tony<br>
MONTGRANDI Jonathan