package com.tonylch.cinedb.fragments

import android.os.Bundle
import androidx.preference.PreferenceFragmentCompat
import com.tonylch.cinedb.R

class AboutFragment: PreferenceFragmentCompat() {

    companion object {
        fun newInstance(): AboutFragment {
            return AboutFragment()
        }
    }

    override fun onCreatePreferences(savedInstanceState: Bundle?, rootKey: String?) {
        addPreferencesFromResource(R.xml.preferences)
    }

}