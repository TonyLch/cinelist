package com.tonylch.cinedb.data.titlesheet

data class Actor(
    val asCharacter: String,
    val id: String,
    val image: String,
    val name: String
)