package com.tonylch.cinedb.data.titlesheet

data class Director(
    val id: String,
    val name: String
)