package com.tonylch.cinedb.data.titlesheet

data class Writer(
    val id: String,
    val name: String
)