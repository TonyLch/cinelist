package com.tonylch.cinedb.data.id

data class Result(
    val description: String,
    val id: String,
    val image: String,
    val resultType: String,
    val title: String
)