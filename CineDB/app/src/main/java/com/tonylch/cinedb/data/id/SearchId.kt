package com.tonylch.cinedb.data.id

data class SearchId(
    val errorMessage: String,
    val expression: String,
    val results: List<Result>,
    val searchType: String
)