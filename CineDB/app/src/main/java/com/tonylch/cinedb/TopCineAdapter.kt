package com.tonylch.cinedb

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import androidx.recyclerview.widget.RecyclerView
import com.squareup.picasso.Picasso
import com.tonylch.cinedb.data.topcine.Item
import com.tonylch.cinedb.data.topcine.TopCine
import kotlinx.android.synthetic.main.item_top_cine.view.*

class TopCineAdapter(private val listTop: List<Item>): RecyclerView.Adapter<TopCineAdapter.ViewHolder>() {

    class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {

        val ivTopCine: ImageView = view.iv_top_cine
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.item_top_cine, parent, false)

        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {

        // Set info in the UI
        Picasso.get()
            .load(listTop[position].image)
            .resize(300, 0)
            .into(holder.ivTopCine)

    }

    override fun getItemCount(): Int = listTop.size

}