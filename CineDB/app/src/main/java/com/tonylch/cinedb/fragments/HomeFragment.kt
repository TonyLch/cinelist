package com.tonylch.cinedb.fragments

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import com.tonylch.cinedb.R
import com.tonylch.cinedb.RequestController
import com.tonylch.cinedb.TopCineAdapter
import com.tonylch.cinedb.data.topcine.Item
import com.tonylch.cinedb.data.topcine.TopCine
import kotlinx.android.synthetic.main.fragment_home.*

class HomeFragment: Fragment() {

    companion object {
        fun newInstance(): HomeFragment {
            return HomeFragment()
        }
    }

    var requestController: RequestController? = null

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        val view: View = inflater!!.inflate(R.layout.fragment_home, container, false)

        // Init ResquestController
        requestController = RequestController()
        requestController?.init()



        return view
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        //val topMovieList = initTopCine(10, true)
        val topMovieList = generateDummyList(10)

        recycler_view_movies.adapter = TopCineAdapter(topMovieList)
        recycler_view_movies.layoutManager = LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false)


        //val topSerieList = initTopCine(10, false)
        val topSerieList = generateDummyList(10)

        recycler_view_series.adapter = TopCineAdapter(topSerieList)
        recycler_view_series.layoutManager = LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false)
    }

    //    private fun initTopCine(size: Int, type: Boolean): List<Item?>? {
//
//        if (type){
//            requestController?.getTopMovies()
//        }
//
//
//        return requestController?.mTopMovieList
//    }

    private fun generateDummyList(size: Int): List<Item> {
        val list = ArrayList<Item>()
        for (i in 0 until size) {
            val drawable = when (i % 3) {
                0 -> "https://imdb-api.com/images/original/MV5BMTMxNTMwODM0NF5BMl5BanBnXkFtZTcwODAyMTk2Mw@@._V1_Ratio0.6716_AL_.jpg"
                1 -> "https://imdb-api.com/images/original/MV5BMTMxNTMwODM0NF5BMl5BanBnXkFtZTcwODAyMTk2Mw@@._V1_Ratio0.6716_AL_.jpg"
                else -> "https://imdb-api.com/images/original/MV5BMTMxNTMwODM0NF5BMl5BanBnXkFtZTcwODAyMTk2Mw@@._V1_Ratio0.6716_AL_.jpg"
            }
            val item = Item("", "", "", "", "", drawable, "", "", "")
            list += item
        }
        return list
    }

}