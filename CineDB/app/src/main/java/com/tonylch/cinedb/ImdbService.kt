package com.tonylch.cinedb

import com.tonylch.cinedb.data.id.SearchId
import com.tonylch.cinedb.data.titlesheet.SearchTitle
import com.tonylch.cinedb.data.topcine.Item
import com.tonylch.cinedb.data.topcine.TopCine
import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Headers
import retrofit2.http.Path

// Request API Endpoints
interface ImdbService {

    // @GET Search Movies Id / Series Id
    // SearchPage
    @Headers("Content-Type: application/json")
    @GET("Search/k_30x4e4za/{name}")
    fun getId(@Path("name") name: String?): Call<SearchId?>?

    // @GET Search Data from Id
    // SearchPage
    @Headers("Content-Type: application/json")
    @GET("Title/k_30x4e4za/{id}")
    fun getData(@Path("id") id: String?): Call<SearchTitle?>?

//    // @GET Top 250 movies (we'll get only the first 10 of them)
//    // HomePage
//    @Headers("Content-Type: application/json")
//    @GET("Top250Movies/k_30x4e4za")
//    fun getTopMovies(): Call<TopCine?>?
//
//    // @GET Top 250 series (we'll get only the first 10 of them)
//    // HomePage
//    @Headers("Content-Type: application/json")
//    @GET("Top250TVs/k_30x4e4za")
//    fun getTopSeries(): Call<TopCine?>?
//
//    // @GET Theater movies
//    // TheaterPage
//    @Headers("Content-Type: application/json")
//    @GET("InTheaters/k_30x4e4za")
//    fun getTheater(): Call<InTheater?>?
}