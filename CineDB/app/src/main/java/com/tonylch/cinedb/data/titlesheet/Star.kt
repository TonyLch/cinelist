package com.tonylch.cinedb.data.titlesheet

data class Star(
    val id: String,
    val name: String
)