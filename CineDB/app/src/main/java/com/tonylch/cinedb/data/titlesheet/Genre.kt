package com.tonylch.cinedb.data.titlesheet

data class Genre(
    val key: String,
    val value: String
)