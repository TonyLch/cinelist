package com.tonylch.cinedb

import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.lifecycle.Lifecycle
import androidx.viewpager2.adapter.FragmentStateAdapter
import com.tonylch.cinedb.fragments.AboutFragment
import com.tonylch.cinedb.fragments.HomeFragment
import com.tonylch.cinedb.fragments.SearchFragment
import com.tonylch.cinedb.fragments.TheaterFragment

class SectionsStateAdapter(fragmentManager: FragmentManager, lifecycle: Lifecycle) :
    FragmentStateAdapter(fragmentManager, lifecycle) {

    private val mFragmentList: ArrayList<Fragment> = arrayListOf(
        HomeFragment(),
        TheaterFragment(),
        SearchFragment(),
        AboutFragment()
    )

    override fun getItemCount(): Int {
        return mFragmentList.size
    }

    override fun createFragment(position: Int): Fragment {
        return mFragmentList[position]
    }
}