package com.tonylch.cinedb.data.topcine

data class TopCine(
    val errorMessage: String,
    val items: List<Item>
)