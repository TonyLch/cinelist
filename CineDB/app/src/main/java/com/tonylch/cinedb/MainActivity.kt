package com.tonylch.cinedb

import android.os.Bundle
import android.util.Log
import androidx.appcompat.app.AppCompatActivity
import androidx.viewpager2.widget.ViewPager2
import com.google.android.material.tabs.TabLayout
import com.google.android.material.tabs.TabLayoutMediator


class MainActivity : AppCompatActivity() {

    // Log TAG
    private val TAG = "MainActivity"

    // VARS
    val SHARED_PREFS: String? = "sharedPrefs"
    val TEXT = "text"
    val SWITCH1 = "switch1"
     var text: String? = null
     var switchOnOff = false

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        // Init tabs
        initViewPager2WithFragments()

    }

    // Init ViewPager with Fragments, add icons to tabs
    private fun initViewPager2WithFragments() {
        Log.d(TAG, "initViewPager2WithFragments() start")

        val viewPager2: ViewPager2 = findViewById(R.id.pager)
        val adapter = SectionsStateAdapter(supportFragmentManager, lifecycle)
        viewPager2.adapter = adapter

        val tabLayout: TabLayout = findViewById(R.id.tab_layout)
        val icons: Array<Int> = arrayOf(R.drawable.ic_home, R.drawable.ic_theater, R.drawable.ic_search, R.drawable.ic_about)
        TabLayoutMediator(tabLayout, viewPager2) {tab, position ->
            tab.setIcon(icons[position])
        }.attach()
    }

}