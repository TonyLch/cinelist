package com.tonylch.cinedb

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import androidx.recyclerview.widget.RecyclerView
import com.squareup.picasso.Picasso
import com.tonylch.cinedb.data.topcine.Item
import kotlinx.android.synthetic.main.item_theater_cine.view.*

class TheaterCineAdapter(private val listTop: List<Item>): RecyclerView.Adapter<TheaterCineAdapter.ViewHolder>() {

    class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {

        val ivTheaterCine: ImageView = view.iv_theater_cine
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.item_theater_cine, parent, false)

        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val currentItem = listTop[position]

        //holder.ivTopCine.setImageResource(currentItem.imageResource)

        // Set info in the UI
        Picasso.get()
            .load(listTop[position].image)
            .resize(300, 0)
            .into(holder.ivTheaterCine)

    }

    override fun getItemCount(): Int = listTop.size

}