package com.tonylch.cinedb

import android.util.Log
import android.widget.ImageView
import android.widget.TextView
import com.squareup.picasso.Picasso
import com.tonylch.cinedb.data.id.Result
import com.tonylch.cinedb.data.id.SearchId
import com.tonylch.cinedb.data.titlesheet.SearchTitle
import com.tonylch.cinedb.data.topcine.Item
import com.tonylch.cinedb.data.topcine.TopCine
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

class RequestController {

    companion object {
        fun newInstance(): RequestController {
            return RequestController()
        }
    }

    private val TAG = "RequestController"

    private val BASE_URL: String = "https://imdb-api.com/en/API/"
    private var apiService: ImdbService? = null
    var mId: String? = null
    var mTopMovieList: List<Item?>? = null
    var mTopSeriesList: List<Item?>? = null

    fun init() {

            val retrofit: Retrofit = Retrofit.Builder()
                .baseUrl(BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build()

            apiService = retrofit.create(ImdbService::class.java)

    }

    fun getId(name: String?) {

        val call: Call<SearchId?>? = apiService?.getId(name)

        call!!.enqueue(object : Callback<SearchId?> {
            override fun onResponse(call: Call<SearchId?>, response: Response<SearchId?>) {
                Log.d(TAG, "onResponse: Server Response: " + response.toString())
                Log.d(TAG, "onResponse: received information: " + response.body().toString())

                val resultsList: List<Result?>? = response.body()?.results
                if (resultsList != null) {
                    for (content in resultsList) {
                        // Log
                        Log.d(
                            TAG, "onResponse: \n" +
                                    "id: " + content?.id + "\n" +
                                    "resultType: " + content?.resultType + "\n" +
                                    "image: " + content?.image + "\n" +
                                    "title: " + content?.title + "\n" +
                                    "description: " + content?.description)

                        mId = resultsList[0]?.id.toString()

                    }
                }
            }
            override fun onFailure(call: Call<SearchId?>, t: Throwable) {
                Log.e(TAG, "onFailure: Something went wrong: " + t.message);
            }
        })

    }


    fun getData(titleId: String?,
              _poster: ImageView, _title: TextView,
              _type: TextView, _year: TextView,
              _releaseDate: TextView, _runTime: TextView,
              _plot: TextView, _directors: TextView,
              _writers: TextView, _actors: TextView) {

        val call: Call<SearchTitle?>? = apiService?.getData(titleId)

        call!!.enqueue(object : Callback<SearchTitle?> {
            override fun onResponse(call: Call<SearchTitle?>, response: Response<SearchTitle?>) {
                Log.d(TAG, "onResponse: Server Response: " + response.toString())
                Log.d(TAG, "onResponse: received information: " + response.body().toString())

                val result: SearchTitle? = response.body()
                if (result != null) {

                    // Set info in the UI
                    Picasso.get()
                        .load(result.image.toString())
                        .resize(500, 0).into(_poster)

                    _title.text = result.title
                    _type.text = result.type
                    _year.text = result.year
                    _releaseDate.text = result.releaseDate
                    _runTime.text = result.runtimeStr
                    _plot.text = result.plot
                    _directors.text = result.directors
                    _writers.text = result.writers
                    _actors.text = result.stars

                }
            }
            override fun onFailure(call: Call<SearchTitle?>, t: Throwable) {
                Log.e(TAG, "onFailure: Something went wrong: " + t.message);
            }
        })

    }

//    fun getTopMovies() {
//
//        val call: Call<TopCine?>? = apiService?.getTopMovies()
//
//        call!!.enqueue(object : Callback<TopCine?> {
//            override fun onResponse(call: Call<TopCine?>, response: Response<TopCine?>) {
//                Log.d(TAG, "onResponse: Server Response: " + response.toString())
//                Log.d(TAG, "onResponse: received information: " + response.body().toString())
//
//                val results: List<Item?>? = response.body()?.items
//                if (results != null) {
//                    for (i in 0..9) {
//                        // Log
//                        Log.d(
//                            TAG, "onResponse: \n" +
//                                    "image: " + results[i]!!.image + "\n" +
//                                    "title: " + results[i]!!.title)
//
//                        //mTopMovieList?.plus(results[i])
//                    }
//
//                }
//            }
//            override fun onFailure(call: Call<TopCine?>, t: Throwable) {
//                Log.e(TAG, "onFailure: Something went wrong: " + t.message);
//            }
//        })
//
//    }

}