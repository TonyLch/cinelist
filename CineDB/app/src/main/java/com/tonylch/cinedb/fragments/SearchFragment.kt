package com.tonylch.cinedb.fragments

import android.os.Bundle
import android.os.Handler
import android.os.Looper
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.Toast
import androidx.fragment.app.Fragment
import com.tonylch.cinedb.R
import com.tonylch.cinedb.RequestController
import kotlinx.android.synthetic.main.fragment_search.*

class SearchFragment: Fragment(), View.OnClickListener {

    companion object {
        fun newInstance(): SearchFragment {
            return SearchFragment()
        }
    }

    var requestController: RequestController? = null

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        val view: View = inflater!!.inflate(R.layout.fragment_search, container, false)

        // Init ResquestController
        requestController = RequestController()
        requestController?.init()

        // Button
        val btn: Button = view.findViewById(R.id.searchBtn)
        btn.setOnClickListener(this)

        return view

    }

    override fun onClick(v: View?) {

        Toast.makeText(context, input.text.toString(), Toast.LENGTH_SHORT).show()

        requestController?.getId(input.text.toString())

        // Delay request
        Handler(Looper.getMainLooper()).postDelayed({
            kotlin.run{
                requestController?.getData(requestController?.mId, poster, title, type, year, release_date, run_time, plot, directors, writers, actors)
            }
        }, 3000)

    }

}